package com.fxclient.server.services;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fxclient.server.models.UserInfo;

@RestController
@RequestMapping(value = "fxclient/login", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
public class LoginService {

	@RequestMapping(value = "login.do", method = RequestMethod.POST)
	public UserInfo doLogin(HttpServletRequest request) {
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		UserInfo v = new UserInfo();
		if ("admin".equals(userName)&&"202cb962ac59075b964b07152d234b70".equals(password)) {
			v.setUserName(userName);
			v.setPassword(password);
		}
		return v;
	}
	
	@RequestMapping(value = "test.do", method = RequestMethod.GET)
	public String test(HttpServletRequest request) {
		return "OK";
	}

}
