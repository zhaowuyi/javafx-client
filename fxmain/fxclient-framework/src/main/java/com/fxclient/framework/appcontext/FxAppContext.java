package com.fxclient.framework.appcontext;

import java.util.ResourceBundle;

import org.springframework.context.ApplicationContext;

import javafx.stage.Stage;

public class FxAppContext {
	private static FxAppContext _fxAppContext;

	private FxAppContext() {

	}

	public static FxAppContext getInstance() {
		if (_fxAppContext == null) {
			_fxAppContext = new FxAppContext();
		}
		return _fxAppContext;
	}

	public ResourceBundle getLanguage() {
		return language;
	}

	public void setLanguage(ResourceBundle language) {
		this.language = language;
	}

	public String getBaseDir() {
		if (baseDir == null) {
			baseDir = System.getProperty("user.dir");
		}
		return baseDir;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}

	public Stage getCurrentStage() {
		return currentStage;
	}

	public void setCurrentStage(Stage currentStage) {
		this.currentStage = currentStage;
	}

	private ResourceBundle language;

	private String baseDir;

	private Stage currentStage;

	private ApplicationContext applicationContext;

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

}
