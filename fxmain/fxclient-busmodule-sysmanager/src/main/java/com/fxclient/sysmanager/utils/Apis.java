package com.fxclient.sysmanager.utils;

import com.fxclient.framework.utils.AppSettings;

public class Apis {
	public static final String QUERY = "fxclient/userManager/query.do";
	public static final String ADD_UPDATE = "fxclient/userManager/addOrUpdate.do";
	public static final String DELETE = "fxclient/userManager/delete.do";
	public static final String EXPORT = "fxclient/userManager/export.do";;
	
	public static String getHttpHost() {
		return "http://" + AppSettings.getInstance().getProperty("serverip") + ":"
				+ AppSettings.getInstance().getProperty("serverPort") + "/";
	}

	public static String getHttpsHost() {
		return "https://" + AppSettings.getInstance().getProperty("serverip") + ":"
				+ AppSettings.getInstance().getProperty("serverPort") + "/";
	}
}
