package com.fxclient.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan(basePackages = "com.fxclient.**.services")
public class APP {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(APP.class, args);
	}

}
